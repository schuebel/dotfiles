"-------------------------------------
" sets 
"-------------------------------------
set tabstop=4 softtabstop=4
set relativenumber nu
syntax on
set backspace=indent,eol,start
set scrolloff=8
set noerrorbells
set nowrap
set incsearch
set completeopt=menuone,noinsert,noselect
set rtp+=/opt/homebrew/bin/
set cmdheight=2
set hidden
" activates filetype detection
filetype plugin on

"-------------------------------------
" plugins
"-------------------------------------
call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'dense-analysis/ale'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'tpope/vim-fugitive'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
call plug#end()

"-------------------------------------
" gruvbox
"-------------------------------------
autocmd vimenter * ++nested colorscheme gruvbox


"-------------------------------------
" remaps
"-------------------------------------
:let mapleader = " "

"Fuzzyfind files
nnoremap <silent> <C-f> :Files<CR>
" window navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" paste from clipboard
nnoremap <C-p> "0p
nnoremap <C-P> "0P
nnoremap <leader>cd :cd %:p:h<CR>
" CoC navigate suggestions
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"-------------------------------------
" misc
"-------------------------------------
"reopen on last line
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif


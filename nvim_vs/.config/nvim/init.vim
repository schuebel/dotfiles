"-------------------------------------
" sets 
"-------------------------------------
set expandtab
set shiftwidth=2
set tabstop=2
set softtabstop=2
set guicursor=
set relativenumber nu
set backspace=indent,eol,start
set scrolloff=8
set noerrorbells
set smartindent
set nowrap
set incsearch
set completeopt=menuone,noinsert,noselect
set rtp+=/opt/homebrew/bin/
set cmdheight=2
set hidden
set signcolumn=yes
set splitbelow
set splitright


"-------------------------------------
" syntax and theme 
"-------------------------------------
" activates filetype detection
syntax on
filetype plugin indent on
" Set the filetype based on the file's extension, overriding any
" 'filetype' that has already been set
au BufRead,BufNewFile *.jenkinsfile set filetype=groovy

" set theme
autocmd vimenter * ++nested colorscheme gruvbox

" pathogen
"execute pathogen#infect()

"-------------------------------------
" misc
"-------------------------------------
" set pwd of window to current file
"autocmd BufEnter * silent! lcd %:p:h
set autochdir

" open file on last know line
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

" trim trialing whitespace
autocmd BufWritePre *.rb :%s/\s\+$//e 

"-------------------------------------
" plugins
"-------------------------------------
call plug#begin()
Plug 'morhetz/gruvbox'
Plug 'dense-analysis/ale'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kyazdani42/nvim-tree.lua'
Plug 'vim-airline/vim-airline'
Plug 'pearofducks/ansible-vim'
call plug#end()

"-------------------------------------
" remaps
"-------------------------------------
:let mapleader = " "

"Fuzzyfind files
nnoremap <silent> <C-f> :Files $HOME/code<CR>
" buffer navigation
nnoremap <silent>    <C-,> <Cmd>:bnext<CR>
nnoremap <silent>    <C-.> <Cmd>:bprevious<CR>
nnoremap gb :ls<CR>:b<Space>
nnoremap <silent> <C-B> :Buffers<CR>

command CD cd $HOME/code 


if status is-interactive
  # enable vi mode
  fish_vi_key_bindings

  # vars
  export FZF_DEFAULT_COMMAND='rg --files --hidden --ignore-file ~/.ignore'
  # Aliases
  alias lg=lazygit
  alias p=python3
  alias vc='NVIM_APPNAME=nvchad_clean nvim'
  alias vim='nvim'
  alias sshus='ssh -o StrictHostKeyChecking=no'
  alias ll="ls -l --color=auto"
  alias la="ls -la --color=auto"
  alias ls='ls --color=auto'
  alias rm='rm -i'
  alias egrep='grep -rn --color=auto . -e'
  alias ..='cd ..'
  alias wttr='curl wttr.in/st-leon-rot'
  alias wttr2="curl -s 'wttr.in/st-leon-rot?format=v2'"
  # alias ff='cd $(dirname `fzf`)'
  # alias fo='vim `fzf`'
  alias sampler='sampler --config ~/.config/sampler/config.yml'


  # FZF config
  # Search for files with ctrl+f
  fzf_configure_bindings --directory=\cf
  # excludes are handled via $HOME/.ignore

  # nvim config switcher
  function dsh
    docker container exec -it $argv[1] /bin/bash
  end

  # function nvchad_clean
  #   env NVIM_APPNAME=nvchad_clean nvim
  # end
  #
  # function nvims
  #     set items nvim nvchad_clean
  #     set config (printf "%s\n" $items | fzf --prompt=" Neovim Config  " --height=~50% --layout=reverse --border --exit-0)
  #     if [ -z $config ]
  #         echo "Nothing selected"
  #         return 0
  #     else if [ $config = "default" ]
  #         set config ""
  #     end
  #     env NVIM_APPNAME=$config nvim $argv
  # end
end

#!/bin/bash
read -p "Do you want to install brew? (yes/no) " ans
if [[ "$ans" = "yes" ]]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

if [[ -f ./Brewfile ]]; then
    echo "This is your brewfile:"
    cat Brewfile
    read -p "Do you want to install everthing listed above? (yes/no) " ans
    if [[ "$ans" = "yes" ]]; then
        brew bundle --file ./Brewfile 
    fi
fi


read -p "Do you want to install oh-my-zsh? (yes/no) " ans
if [[ "$ans" = "yes" ]]; then
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi


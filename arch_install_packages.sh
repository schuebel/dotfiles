#!/bin/bash
if [ -z $1 ]; then
	echo '
Please provide a file that contains a list of packages to be installed.
Usage: 

install_packages.sh [FILE]
'
exit 1
fi

sudo pacman -S --needed $(comm -12 <(pacman -Slq|sort) <(sort $1) )

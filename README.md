# dotfiles

This repository contains important config files and a list of installed packages.

## config files
Can be installed with `install.sh`. Some require root permission (installed in `/etc`).

## packages
`pkglist.txt` contains a list of packages that have been explicitly installed. To make sure all packages in the list are installed run:

```bash
sudo pacman -S --needed $(comm -12 <(pacman -Slq|sort) <(sort pkg_list.txt) )

```
